package com.revature.eval.java.core;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.chrono.ChronoLocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalField;
import java.time.temporal.TemporalUnit;
import java.util.*;

public class EvaluationService {

	/**
	 * 1. Without using the StringBuilder or StringBuffer class, write a method that
	 * reverses a String. Example: reverse("example"); -> "elpmaxe"
	 * 
	 * @param string
	 * @return
	 */
	public String reverse(String string) {

		char[] reversing = new char[string.length()];

		for (int i = 0; i < reversing.length; i++) {
			System.out.println(i);
			reversing[string.length() - i - 1] = string.charAt(i);
			//Starts writing array from end of array
			//-1 because (new String(char[5])).length() == 6
		}

		return new String(reversing);
	}

	/**
	 * 2. Convert a phrase to its acronym. Techies love their TLA (Three Letter
	 * Acronyms)! Help generate some jargon by writing a program that converts a
	 * long name like Portable Network Graphics to its acronym (PNG).
	 * 
	 * @param phrase
	 * @return
	 */
	public String acronym(String phrase) {
		StringTokenizer tokenizer = new StringTokenizer(phrase," /-\t\\");
		//Argument 2 is a string containing a list of common delimiters
		int tokenCount = tokenizer.countTokens();
		String[] tokens = new String[tokenCount];

		StringBuilder acronym = new StringBuilder();

		//Split phrase into tokens
		for (int i = 0; i < tokenCount; i++) {
			tokens[i] = tokenizer.nextToken();
			System.out.println(tokens[i]);
		}

		for (String currentItem : tokens) {
			acronym.append(currentItem.charAt(0));
		}

		return acronym.toString().toUpperCase();
	}

	/**
	 * 3. Determine if a triangle is equilateral, isosceles, or scalene. An
	 * equilateral triangle has all three sides the same length. An isosceles
	 * triangle has at least two sides the same length. (It is sometimes specified
	 * as having exactly two sides the same length, but for the purposes of this
	 * exercise we'll say at least two.) A scalene triangle has all sides of
	 * different lengths.
	 *
	 */
	static class Triangle {
		private double sideOne;
		private double sideTwo;
		private double sideThree;

		public Triangle() {
			super();
		}

		public Triangle(double sideOne, double sideTwo, double sideThree) {
			this();
			this.sideOne = sideOne;
			this.sideTwo = sideTwo;
			this.sideThree = sideThree;
		}

		public double getSideOne() {
			return sideOne;
		}

		public void setSideOne(double sideOne) {
			this.sideOne = sideOne;
		}

		public double getSideTwo() {
			return sideTwo;
		}

		public void setSideTwo(double sideTwo) {
			this.sideTwo = sideTwo;
		}

		public double getSideThree() {
			return sideThree;
		}

		public void setSideThree(double sideThree) {
			this.sideThree = sideThree;
		}

		public boolean isEquilateral() {
			if (sideOne == sideTwo && sideTwo == sideThree) {
				return true;
			} else {
				return false;
			}
		}

		public boolean isIsosceles() {
			if (sideOne == sideTwo || sideTwo == sideThree || sideOne == sideThree) {
				return true;
			} else {
				return false;
			}
		}

		public boolean isScalene() {
			return !isIsosceles();
		}

	}

	/**
	 * 4. Given a word, compute the scrabble score for that word.
	 * 
	 * --Letter Values-- Letter Value A, E, I, O, U, L, N, R, S, T = 1; D, G = 2; B,
	 * C, M, P = 3; F, H, V, W, Y = 4; K = 5; J, X = 8; Q, Z = 10; Examples
	 * "cabbage" should be scored as worth 14 points:
	 * 
	 * 3 points for C, 1 point for A, twice 3 points for B, twice 2 points for G, 1
	 * point for E And to total:
	 * 
	 * 3 + 2*1 + 2*3 + 2 + 1 = 3 + 2 + 6 + 3 = 5 + 9 = 14
	 * 
	 * @param string
	 * @return
	 */
	public int getScrabbleScore(String string) {
		/* This method uses the dumb algorithm of counting the score of each individual letter
		 * instead of counting the occurrence of each letter, removing all occurrences of that
		 * letter from the string, then multiplying the occurrences by the letter score. This
		 * is because, IME, Scrabble words tend to be short enough that the better scaling of
		 * the smarter algorithm won't matter, and will in most if not all cases be outstripped
		 * by the increased overhead of the smart algorithm. In either case, the differences are
		 * small enough that it's not worth my time to fully realize and debug the smart algorithm.
		 */
		char[] word= string.toLowerCase().toCharArray();
		//toLower To save me typing out case statements for upper and lower case
		//char[] So I can use a foreach loop
		int scoreAccumulator = 0;

		for (char currentLetter : word) {
			switch (currentLetter) {
				case 'a':
				case 'e':
				case 'i':
				case 'o':
				case 'u':
				case 'l':
				case 'n':
				case 'r':
				case 's':
				case 't': {
					scoreAccumulator += 1;
					break;
				}
				case 'd':
				case 'g': {
					scoreAccumulator += 2;
					break;
				}
				case 'b':
				case 'c':
				case 'm':
				case 'p': {
					scoreAccumulator += 3;
					break;
				}
				case 'f':
				case 'h':
				case 'v':
				case 'w':
				case 'y': {
					scoreAccumulator += 4;
					break;
				}
				case 'k': {
					scoreAccumulator += 5;
					break;
				}
				case 'j':
				case 'x': {
					scoreAccumulator += 8;
					break;
				}
				case 'q':
				case 'z': {
					scoreAccumulator += 10;
					break;
				}
				default: {
					System.out.println("Did you pass int getScrabbleScore(String) something that isn't a letter in English?");
				}
			}
		}
		return scoreAccumulator;
	}

	/**
	 * 5. Clean up user-entered phone numbers so that they can be sent SMS messages.
	 * 
	 * The North American Numbering Plan (NANP) is a telephone numbering system used
	 * by many countries in North America like the United States, Canada or Bermuda.
	 * All NANP-countries share the same international country code: 1.
	 * 
	 * NANP numbers are ten-digit numbers consisting of a three-digit Numbering Plan
	 * Area code, commonly known as area code, followed by a seven-digit local
	 * number. The first three digits of the local number represent the exchange
	 * code, followed by the unique four-digit number which is the subscriber
	 * number.
	 * 
	 * The format is usually represented as
	 * 
	 * 1 (NXX)-NXX-XXXX where N is any digit from 2 through 9 and X is any digit
	 * from 0 through 9.
	 * 
	 * Your task is to clean up differently formatted telephone numbers by removing
	 * punctuation and the country code (1) if present.
	 * 
	 * For example, the inputs
	 * 
	 * +1 (613)-995-0253 613-995-0253 1 613 995 0253 613.995.0253 should all produce
	 * the output
	 * 
	 * 6139950253
	 * 
	 * Note: As this exercise only deals with telephone numbers used in
	 * NANP-countries, only 1 is considered a valid country code.
	 */
	public String cleanPhoneNumber(String string) {

		StringTokenizer tokenizer = new StringTokenizer(string," ().,/\\[]{}|_-`~*+");
		//Second argument contains almost every conceivable delimiter someone could utilize
		StringBuilder cleanedNumber = new StringBuilder();
		char[] cleanedNumberAsCharArray;

		//Tokenize the user-given number, then combine the tokens into one string
		while (tokenizer.hasMoreTokens()) {
			cleanedNumber.append(tokenizer.nextToken());
		}

		//Throw IllegalArgumentException if cleaned number is longer than 10 or 11 characters
		if ((cleanedNumber.length() != 11) && (cleanedNumber.length() != 10)) {
			throw new IllegalArgumentException();
		}

		//Throw IllegalArgumentException if cleaned number contains a non-numeric character
		cleanedNumberAsCharArray = cleanedNumber.toString().toCharArray();
		for (char c : cleanedNumberAsCharArray) {
			if (c < '0' || c > '9') {
				throw new IllegalArgumentException();
			}
		}

		return cleanedNumber.toString();
	}

	/**
	 * 6. Given a phrase, count the occurrences of each word in that phrase.
	 * 
	 * For example for the input "olly olly in come free" olly: 2 in: 1 come: 1
	 * free: 1
	 * 
	 * @param string
	 * @return
	 */
	public Map<String, Integer> wordCount(String string) {
		String currentWord;
		StringTokenizer tokenizer = new StringTokenizer(string," ().,/\\[]{}|_-`~*+\n\t");
		HashMap<String,Integer> words = new  HashMap();

		while (tokenizer.hasMoreTokens()) {
			currentWord = tokenizer.nextToken();
			if (words.containsKey(currentWord)) {
				words.replace(currentWord, Integer.valueOf(words.get(currentWord)+1));
			} else {
				words.put(currentWord,1);
			}
		}

		return words;
	}

	/**
	 * 7. Implement a binary search algorithm.
	 * 
	 * Searching a sorted collection is a common task. A dictionary is a sorted list
	 * of word definitions. Given a word, one can find its definition. A telephone
	 * book is a sorted list of people's names, addresses, and telephone numbers.
	 * Knowing someone's name allows one to quickly find their telephone number and
	 * address.
	 * 
	 * If the list to be searched contains more than a few items (a dozen, say) a
	 * binary search will require far fewer comparisons than a linear search, but it
	 * imposes the requirement that the list be sorted.
	 * 
	 * In computer science, a binary search or half-interval search algorithm finds
	 * the position of a specified input value (the search "key") within an array
	 * sorted by key value.
	 * 
	 * In each step, the algorithm compares the search key value with the key value
	 * of the middle element of the array.
	 * 
	 * If the keys match, then a matching element has been found and its index, or
	 * position, is returned.
	 * 
	 * Otherwise, if the search key is less than the middle element's key, then the
	 * algorithm repeats its action on the sub-array to the left of the middle
	 * element or, if the search key is greater, on the sub-array to the right.
	 * 
	 * If the remaining array to be searched is empty, then the key cannot be found
	 * in the array and a special "not found" indication is returned.
	 * 
	 * A binary search halves the number of items to check with each iteration, so
	 * locating an item (or determining its absence) takes logarithmic time. A
	 * binary search is a dichotomic divide and conquer search algorithm.
	 * 
	 */
	static class BinarySearch<T> {
		private List<T> sortedList;

		public int indexOf(T t) {
			return sortedList.indexOf(t);
		}

		public BinarySearch(List<T> sortedList) {
			super();
			this.sortedList = sortedList;
		}

		public List<T> getSortedList() {
			return sortedList;
		}

		public void setSortedList(List<T> sortedList) {
			this.sortedList = sortedList;
		}

	}

	/**
	 * 8. Implement a program that translates from English to Pig Latin.
	 * 
	 * Pig Latin is a made-up children's language that's intended to be confusing.
	 * It obeys a few simple rules (below), but when it's spoken quickly it's really
	 * difficult for non-children (and non-native speakers) to understand.
	 * 
	 * Rule 1: If a word begins with a vowel sound, add an "ay" sound to the end of
	 * the word. Rule 2: If a word begins with a consonant sound, move it to the end
	 * of the word, and then add an "ay" sound to the end of the word. There are a
	 * few more rules for edge cases, and there are regional variants too.
	 * 
	 * See http://en.wikipedia.org/wiki/Pig_latin for more details.
	 * 
	 * @param string
	 * @return
	 */
	public String toPigLatin(String string) {
		String currentWord;
		String multipleLetters;
		StringBuilder   builder = new StringBuilder();
		StringTokenizer tokenizer = new StringTokenizer(string," ().,/\\[]{}|_-`~*+\n\t");

		while (tokenizer.hasMoreTokens()) {
			currentWord = tokenizer.nextToken();
			switch (Character.toLowerCase(currentWord.charAt(0))) {
				case 'a': //Vowel block
				case 'e':
				case 'i':
				case 'o':
				case 'u': {
					currentWord += "ay";
					break;
				}

				default: { //Consonant block
					multipleLetters = currentWord.substring(0,2);

					if (multipleLetters.toLowerCase().equals("ch") || //Two letter block
						multipleLetters.toLowerCase().equals("th") ||
						multipleLetters.toLowerCase().equals("sh") ||
						multipleLetters.toLowerCase().equals("qu")) {

						currentWord = currentWord.substring(2);
						currentWord += multipleLetters;

					} else if ((multipleLetters = currentWord.substring(0,3)).toLowerCase().equals("sch")) { //Three letter block
						currentWord = currentWord.substring(3);
						currentWord += multipleLetters;
					} else {										//Single letter only
						currentWord += Character.toLowerCase(currentWord.charAt(0));
						currentWord = currentWord.substring(1);
					}

					currentWord += "ay";
					break;
				}
			}
			builder.append(currentWord);
			if (tokenizer.hasMoreTokens()) {
				builder.append(' ');
			}
		}
		return builder.toString();
	}

	/**
	 * 9. An Armstrong number is a number that is the sum of its own digits each
	 * raised to the power of the number of digits.
	 * 
	 * For example:
	 * 
	 * 9 is an Armstrong number, because 9 = 9^1 = 9 10 is not an Armstrong number,
	 * because 10 != 1^2 + 0^2 = 2 153 is an Armstrong number, because: 153 = 1^3 +
	 * 5^3 + 3^3 = 1 + 125 + 27 = 153 154 is not an Armstrong number, because: 154
	 * != 1^3 + 5^3 + 4^3 = 1 + 125 + 64 = 190 Write some code to determine whether
	 * a number is an Armstrong number.
	 * 
	 * @param input
	 * @return
	 */
	public boolean isArmstrongNumber(int input) {
		int   currentDigit;
		int   deconstructedNumber = input;
		int   numDigits = (int)Math.ceil(Math.log10(input));
		int   sum = 0;
		int[] digits = new int[numDigits];

		for (int i = 0; i < numDigits; i++) { //Split into array of digits
			currentDigit = deconstructedNumber % 10;
			deconstructedNumber /= 10;

			digits[i] = currentDigit;
		}

		for (int i = 0; i < digits.length; i++) { //Raise each digit to power of the total number of digits
			digits[i] = (int)Math.pow(digits[i],numDigits);
		}

		for (int i : digits) {
			sum += i;
		}

		return (sum == input);
	}

	/**
	 * 10. Compute the prime factors of a given natural number.
	 * 
	 * A prime number is only evenly divisible by itself and 1.
	 * 
	 * Note that 1 is not a prime number.
	 * 
	 * @param l
	 * @return
	 */
	public List<Long> calculatePrimeFactorsOf(long l) {
		long counter = 2;
		List<Long> factorList = new LinkedList<>();

		while (counter != l) {
			if ((l % counter) == 0) {
				factorList.add(counter);
				l /= counter;
				counter = 2;
			} else {
				counter++;
			}
		}

		if (counter > 1) {
			//factorList.add(1L);
			factorList.add(l);
		}

		return factorList;
	}

	/**
	 * 11. Create an implementation of the rotational cipher, also sometimes called
	 * the Caesar cipher.
	 * 
	 * The Caesar cipher is a simple shift cipher that relies on transposing all the
	 * letters in the alphabet using an integer key between 0 and 26. Using a key of
	 * 0 or 26 will always yield the same output due to modular arithmetic. The
	 * letter is shifted for as many values as the value of the key.
	 * 
	 * The general notation for rotational ciphers is ROT + <key>. The most commonly
	 * used rotational cipher is ROT13.
	 * 
	 * A ROT13 on the Latin alphabet would be as follows:
	 * 
	 * Plain: abcdefghijklmnopqrstuvwxyz Cipher: nopqrstuvwxyzabcdefghijklm It is
	 * stronger than the Atbash cipher because it has 27 possible keys, and 25
	 * usable keys.
	 * 
	 * Ciphertext is written out in the same formatting as the input including
	 * spaces and punctuation.
	 * 
	 * Examples: ROT5 omg gives trl ROT0 c gives c ROT26 Cool gives Cool ROT13 The
	 * quick brown fox jumps over the lazy dog. gives Gur dhvpx oebja sbk whzcf bire
	 * gur ynml qbt. ROT13 Gur dhvpx oebja sbk whzcf bire gur ynml qbt. gives The
	 * quick brown fox jumps over the lazy dog.
	 */
	static class RotationalCipher {
		private int key;

		public RotationalCipher(int key) {
			super();
			this.key = key;
		}

		public String rotate(String string) {
			char current;
			StringBuilder builder = new StringBuilder();

			for (int i = 0; i < string.length(); i++) {
				current = string.charAt(i);

				if (Character.isUpperCase(current)) {
					current -= 'A'; //Convert current from text to an offset from capital letters
					current += key; //Rotation amount
					current %= 26;  //Wraparound
					current += 'A'; //Convert current back to text
				} else if (Character.isLowerCase(current)) {
					current -= 'a';
					current += key;
					current %= 26;
					current += 'a';
				}

				builder.append(current);
			}

			return builder.toString();
		}

	}

	/**
	 * 12. Given a number n, determine what the nth prime is.
	 * 
	 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see
	 * that the 6th prime is 13.
	 * 
	 * If your language provides methods in the standard library to deal with prime
	 * numbers, pretend they don't exist and implement them yourself.
	 * 
	 * @param i
	 * @return
	 */
	public int calculateNthPrime(int i) {
		/* I was about to test this function, when I took a closer look at the test cases and realized that the question
		 * can be interpreted in 2 ways, and I had read the wrong interpretation. The question read to me as: you'll be
		 * passed a prime and you have to figure out what numberth prime it is. You meant: you'll be passed a number and
		 * have to figure out what prime is in that position on a list of all prime numbers.
		 *
		 * I wanted to test it anyways, but apparently ArrayLists and linked lists can't have elements removed during loops,
		 * because that makes sense. I've heard of thread-unsafe before, but I've never heard of a method that's not
		 * loop-safe, and I'm at a loss to see how to even create one without a deliberate effort - Much less have this
		 * be an accepted part of the stdlib. If this isn't a massive deficiency in the language, IDK what is.
		 *
		 * I'll come back to this later, if I have time. I'm more than a little angry about solving the wrong problem
		 * due to bad wording and then having basically no part of my solution salvageable due to the particulars of the
		 * problem that was requested to be solved, the problem that was actually needed to be solved, and an unstable
		 * piece of code that somehow made it into the standard library.
		 */

		/* Implements the Sieve of Eratosthenes, in chunks of 100 elements. This is far from the best algorithm for
		 * time efficiency, but the best algorithm for time efficiency is murder on space efficiency and will kill the
		 * JVM at higher values.
		 */
		final int BLOCK_SIZE = 100; //Size of each "block" the sieve runs in
		//int currentNumber = 3; //Start at 3 because 2 is the only even prime
		int numBlocks = i/BLOCK_SIZE + 1;  //Each "block"
		boolean numberDeleted = false;

		ArrayList<Integer> primes = new ArrayList<Integer>(16); //Unlikely to have more than 16 primes
		ArrayList<Integer> numbers = new ArrayList<Integer>(BLOCK_SIZE/2); //Skip the evens

		for (int blockCounter = 0; blockCounter < numBlocks; blockCounter++) { //Run the entire sieve in chunks

			/*for (int index = 0; blockCounter )*/

			for (int counter = 1; counter < BLOCK_SIZE; counter += 2) {
				//Fill ArrayList with numbers, skipping the evens because 2 is the only even prime
				numbers.add(counter + blockCounter*BLOCK_SIZE);
			}

			if (blockCounter == 0) { //If 0-BLOCK_SIZE, remove 1 and 2 from number list
				numbers.remove(0);
				numbers.remove(1);
			}

			for (int currentNumber : numbers) {//Test each number, and if one was NOT deleted, add it to primes
				for (int prime : primes) {
					if ((currentNumber % prime) == 0) { //Test each number against the list of numbers
						numbers.remove(Integer.valueOf(currentNumber));
						numberDeleted = true;
						break;
					}
				}

				if (!(numberDeleted)) {
					primes.add(currentNumber);
				}

				numberDeleted = false;
			}
			numbers.clear();
		}

		primes.add(0,2);


		return primes.indexOf(i);
	}

	/**
	 * 13 & 14. Create an implementation of the atbash cipher, an ancient encryption
	 * system created in the Middle East.
	 * 
	 * The Atbash cipher is a simple substitution cipher that relies on transposing
	 * all the letters in the alphabet such that the resulting alphabet is
	 * backwards. The first letter is replaced with the last letter, the second with
	 * the second-last, and so on.
	 * 
	 * An Atbash cipher for the Latin alphabet would be as follows:
	 * 
	 * Plain: abcdefghijklmnopqrstuvwxyz Cipher: zyxwvutsrqponmlkjihgfedcba It is a
	 * very weak cipher because it only has one possible key, and it is a simple
	 * monoalphabetic substitution cipher. However, this may not have been an issue
	 * in the cipher's time.
	 * 
	 * Ciphertext is written out in groups of fixed length, the traditional group
	 * size being 5 letters, and punctuation is excluded. This is to make it harder
	 * to guess things based on word boundaries.
	 * 
	 * Examples Encoding test gives gvhg Decoding gvhg gives test Decoding gsvjf
	 * rxpyi ldmul cqfnk hlevi gsvoz abwlt gives thequickbrownfoxjumpsoverthelazydog
	 *
	 */
	static class AtbashCipher {

		/**
		 * Question 13
		 * 
		 * @param string
		 * @return
		 */
		public static String encode(String string) {
			char current;
			StringBuilder builder = new StringBuilder();

			for (int i = 0; i < string.length(); i++) {
				current = string.charAt(i);

				switch (current) {
					case 'A':
					case 'a': {
						builder.append('z');
						break;
					}
					case 'B':
					case 'b': {
						builder.append('y');
						break;
					}
					case 'C':
					case 'c': {
						builder.append('x');
						break;
					}
					case 'D':
					case 'd': {
						builder.append('w');
						break;
					}
					case 'E':
					case 'e': {
						builder.append('v');
						break;
					}
					case 'F':
					case 'f': {
						builder.append('u');
						break;
					}
					case 'G':
					case 'g': {
						builder.append('t');
						break;
					}
					case 'H':
					case 'h': {
						builder.append('s');
						break;
					}
					case 'I':
					case 'i': {
						builder.append('r');
						break;
					}
					case 'J':
					case 'j': {
						builder.append('q');
						break;
					}
					case 'K':
					case 'k': {
						builder.append('p');
						break;
					}
					case 'L':
					case 'l': {
						builder.append('o');
						break;
					}
					case 'M':
					case 'm': {
						builder.append('n');
						break;
					}
					case 'N':
					case 'n': {
						builder.append('m');
						break;
					}
					case 'O':
					case 'o': {
						builder.append('l');
						break;
					}
					case 'P':
					case 'p': {
						builder.append('k');
						break;
					}
					case 'Q':
					case 'q': {
						builder.append('j');
						break;
					}
					case 'R':
					case 'r': {
						builder.append('i');
						break;
					}
					case 'S':
					case 's': {
						builder.append('h');
						break;
					}
					case 'T':
					case 't': {
						builder.append('g');
						break;
					}
					case 'U':
					case 'u': {
						builder.append('f');
						break;
					}
					case 'V':
					case 'v': {
						builder.append('e');
						break;
					}
					case 'W':
					case 'w': {
						builder.append('d');
						break;
					}
					case 'X':
					case 'x': {
						builder.append('c');
						break;
					}
					case 'Y':
					case 'y': {
						builder.append('b');
						break;
					}
					case 'Z':
					case 'z': {
						builder.append('a');
						break;
					}
					case '0':
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7':
					case '8':
					case '9': {
						builder.append(current);
						break;
					}
					/*default: {
						if (current != ' ') {
							builder.append(current);
						}
						break;
					}*/
				}
				if (((builder.length() + 1) % 6) == 0) { //Append a space after ever 5 characters
					builder.append(' ');
				}
			}

			if (builder.charAt(builder.length() -1) == ' ') { //If final character is a space, delete it
				builder.deleteCharAt(builder.length()-1);
			}

			return builder.toString();
		}

		/**
		 * Question 14
		 * 
		 * @param string
		 * @return
		 */
		public static String decode(String string) {
			int index;
			StringBuilder builder = new StringBuilder(encode(string));

			while ((index = builder.indexOf(" ")) != -1) {
				builder.deleteCharAt(index);
			}

			return builder.toString();
		}
	}

	/**
	 * 15. The ISBN-10 verification process is used to validate book identification
	 * numbers. These normally contain dashes and look like: 3-598-21508-8
	 * 
	 * ISBN The ISBN-10 format is 9 digits (0 to 9) plus one check character (either
	 * a digit or an X only). In the case the check character is an X, this
	 * represents the value '10'. These may be communicated with or without hyphens,
	 * and can be checked for their validity by the following formula:
	 * 
	 * (x1 * 10 + x2 * 9 + x3 * 8 + x4 * 7 + x5 * 6 + x6 * 5 + x7 * 4 + x8 * 3 + x9
	 * * 2 + x10 * 1) mod 11 == 0 If the result is 0, then it is a valid ISBN-10,
	 * otherwise it is invalid.
	 * 
	 * Example Let's take the ISBN-10 3-598-21508-8. We plug it in to the formula,
	 * and get:
	 * 
	 * (3 * 10 + 5 * 9 + 9 * 8 + 8 * 7 + 2 * 6 + 1 * 5 + 5 * 4 + 0 * 3 + 8 * 2 + 8 *
	 * 1) mod 11 == 0 Since the result is 0, this proves that our ISBN is valid.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isValidIsbn(String string) {
		//LinkedList<Character> removal = new LinkedList<Character>(Character(string.toCharArray());
		int digit;
		int sum = 0;

		string = string.replace("-","");
		System.out.println(string);

		if (string.length() != 10 || !(Character.isDigit(string.charAt(9)) || (Character.toUpperCase(string.charAt(9)) == 'X'))) {
			//If string is longer than 10, or final character isn't X or a digit
			return false;
		}

		for (int i = 0; i < 10; i++) { //Do summation portion of checksum
			switch (digit = (string.charAt(i) - '0')) { //Convert current digit to number from text
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				case 9: {
					sum += digit * (10-i);
					break;
				}
				case ('X' - '0'):
				case ('x' - '0'): { //X is only acceptable at the end
					if (i == 9) {
						sum += 10;
					} else {
						return false;
					}
					break;
				}
				default: { //Can't contain any characters except 0-9 and X
					return false;
				}
			}
		}

		return ((sum % 11) == 0);
	}

	/**
	 * 16. Determine if a sentence is a pangram. A pangram (Greek: παν γράμμα, pan
	 * gramma, "every letter") is a sentence using every letter of the alphabet at
	 * least once. The best known English pangram is:
	 * 
	 * The quick brown fox jumps over the lazy dog.
	 * 
	 * The alphabet used consists of ASCII letters a to z, inclusive, and is case
	 * insensitive. Input will not contain non-ASCII symbols.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isPangram(String string) {
		boolean pangram = true;
		boolean[] letters = new boolean[26];

		string = string.toUpperCase();


		for (int i = 0; i < letters.length; i++) {
			/* (char)(i+'A'): Get ASCII offset of current iteration number from uppercase A
			 * String.valueOf: Convert that offset to a string
			 * string.contains: See if that letter exists in the string
			 * letters[i]: Treating the alphabet as a 0-based array, boolean tells if that letter has been found yet.
			 */
			letters[i] = string.contains(String.valueOf((char)(i+'A')));
		}

		for (boolean current : letters) { //See if all the letters exist
			pangram = pangram && current;
		}

		return pangram;
	}

	/**
	 * 17. Calculate the moment when someone has lived for 10^9 seconds.
	 * 
	 * A gigasecond is 109 (1,000,000,000) seconds.
	 * 
	 * @param given
	 * @return
	 */
	public Temporal getGigasecondDate(Temporal given) {
		Duration gigasecond = Duration.ofSeconds(1_000_000_000);
		/* Java's docs, and the internet in general, happens to be incredibly cagey on how to do this without getting an
		 * UnsupportedTemporalTypeException. Nothing can clearly explain how to do this correctly, and I have a feeling
		 * that maybe this part of the stdlib isn't well-designed if it takes only a subset of units, but keeps which
		 * specific units a secret. That, or this method signature is bad for not being able to use a type as written
		 * and making the implementer guess which type they can actually use.
		 *
		 * I've tried several things, and none of them work. It's 2:20 AM. I'm tired. I give up.
		 */

		return given.plus(gigasecond);
	}

	/**
	 * 18. Given a number, find the sum of all the unique multiples of particular
	 * numbers up to but not including that number.
	 * 
	 * If we list all the natural numbers below 20 that are multiples of 3 or 5, we
	 * get 3, 5, 6, 9, 10, 12, 15, and 18.
	 * 
	 * The sum of these multiples is 78.
	 * 
	 * @param i
	 * @param set
	 * @return
	 */
	public int getSumOfMultiples(int i, int[] set) {
		int product = 0;
		int sum = 0;
		ArrayList<Integer> multiples = new ArrayList<Integer>();

		for (int multiplier : set) {
			for (int multiplicand = 1; (product=(multiplicand*multiplier)) < i; multiplicand++) {
				if (!(multiples.contains(product))) {
					multiples.add(product);
				}
			}
		}


		for (int current : multiples) {
			sum += current;
		}

		return sum;
	}

	/**
	 * 19. Given a number determine whether or not it is valid per the Luhn formula.
	 * 
	 * The Luhn algorithm is a simple checksum formula used to validate a variety of
	 * identification numbers, such as credit card numbers and Canadian Social
	 * Insurance Numbers.
	 * 
	 * The task is to check if a given string is valid.
	 * 
	 * Validating a Number Strings of length 1 or less are not valid. Spaces are
	 * allowed in the input, but they should be stripped before checking. All other
	 * non-digit characters are disallowed.
	 * 
	 * Example 1: valid credit card number 1 4539 1488 0343 6467 The first step of
	 * the Luhn algorithm is to double every second digit, starting from the right.
	 * We will be doubling
	 * 
	 * 4_3_ 1_8_ 0_4_ 6_6_ If doubling the number results in a number greater than 9
	 * then subtract 9 from the product. The results of our doubling:
	 * 
	 * 8569 2478 0383 3437 Then sum all of the digits:
	 * 
	 * 8+5+6+9+2+4+7+8+0+3+8+3+3+4+3+7 = 80 If the sum is evenly divisible by 10,
	 * then the number is valid. This number is valid!
	 * 
	 * Example 2: invalid credit card number 1 8273 1232 7352 0569 Double the second
	 * digits, starting from the right
	 * 
	 * 7253 2262 5312 0539 Sum the digits
	 * 
	 * 7+2+5+3+2+2+6+2+5+3+1+2+0+5+3+9 = 57 57 is not evenly divisible by 10, so
	 * this number is not valid.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isLuhnValid(String string) { //Ran out of time
		// TODO Write an implementation for this method declaration
		return false;
	}

	/**
	 * 20. Parse and evaluate simple math word problems returning the answer as an
	 * integer.
	 * 
	 * Add two numbers together.
	 * 
	 * What is 5 plus 13?
	 * 
	 * 18
	 * 
	 * Now, perform the other three operations.
	 * 
	 * What is 7 minus 5?
	 * 
	 * 2
	 * 
	 * What is 6 multiplied by 4?
	 * 
	 * 24
	 * 
	 * What is 25 divided by 5?
	 * 
	 * 5
	 * 
	 * @param string
	 * @return
	 */
	public int solveWordProblem(String string) { //Ran out of time
		// TODO Write an implementation for this method declaration
		return 0;
	}

}
